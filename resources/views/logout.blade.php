<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>kapal jodoh</title>
  </head>
  <body>

  <div class="panel-header">
    <div class="container mt-2">
      <h1><marquee ><strong>Terima Kasih Atas Kunjungannya</strong></marquee></h1>
    </div>
  </div>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Kapal Jodoh</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="/">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="dua">Daftar Pesanan</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('pesanan')}}">Pesanan</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="tiga">Logout</a>
          </li>
      </ul>
    </div>
  </div>
</nav>
<div class="container mt-3">
<center><img src="image/jk.jpg" class="img-fluid" alt="..."></center> 
</div>
  <div class="container">
        <div class="judul text-center mt-3">
            <h3 class="font-weight-bold"><var>KAPAL JODOH</var></h3>
            <h5><i>Jl.Raya Tamberu, Batu Bintang, Batumarmar, Pamekasan, Jawa Timur<br> Buka Jam<strong> 07:00 - 22:00</strong></i> </h5>
        </div>
        
    </div>
    <hr class="footer">
      <div class="container">
        <div class="col text-center">
          
            <div class="Copyright"><var><strong>Copyright</strong><i class=" var fa-Copyright "></i> 2021 - Designed by Norfiah Lailatin~NS</var></p>
                     </div>
         
        </div>
      </div>

   
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

  </body>
</html>