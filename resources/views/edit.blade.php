<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <title>kapal jodoh</title>
  </head>
  <body>

  <div class="panel-header">
    <div class="container">
      <h1><marquee ><strong>Welcome To Kapal Jodoh</strong></marquee></h1>
    </div>
  </div>


<div class="container mt-5">
<div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
    <ol class="carousel-indicators">
      <li data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active"></li>
      <li data-bs-target="#carouselExampleDark" data-bs-slide-to="1"></li>
      <li data-bs-target="#carouselExampleDark" data-bs-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <div class="carousel-item active" >
       <center><img src="{{ asset('image/KL.png') }}" class="d-block w-70" alt="First slide image"></center> 
        <div class="carousel-caption d-none d-md-block">
          <h5>WISATA KAPAL JODOH</h5>
          <p><strong>Silahkan Pilih Menu Sesuai Yang Anda Inginkan <br> Enjoy Your Meal</strong> </p>
        </div>
      </div>
      <div class="carousel-item" >
        <center><img src="{{ asset('image/KL.png') }}" class="d-block w-70" alt="Second slide image"></center>
        <div class="carousel-caption d-none d-md-block">
          <h5>WISATA KAPAL JODOH</h5>
          <p>Silahkan Pilih Menu Sesuai Yang Anda Inginkan <br> Enjoy Your Meal </p>
        </div>
      </div>
      <div class="carousel-item">
        <center><img src="{{ asset('image/HG.png') }}" class="d-block w-70" alt="Third slide image"></center>
        <div class="carousel-caption d-none d-md-block">
          <h5>WISATA KAPAL JODOH</h5>
          <p>Silahkan Pilih Menu Sesuai Yang Anda Inginkan <br> Enjoy Your Meal </p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleDark" role="button" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleDark" role="button" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </a>
  </div><br>
  </div>

    <section>
        <div class="content">
            <div class="card card-info card-outline">
                <div class="card-header">
                    <h3 class="alert alert-primary text-center mt-3">Ubah Pesanan Anda</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('edit' , $data->id) }}" method="POST">
                    @csrf
                    @method('put')
                    <div class="from-group">
                            <input type="text" id="no" name="no" class="form-control" placeholder="Nomer Meja">
                        </div>
                        <div class="from-group mt-2">
                            <input type="text" id="nama" name="nama" class="form-control" placeholder="Nama Pesanan">
                        </div>
                        <div class="from-group mt-2">
                            <input type="text" id="harga" name="harga" class="form-control" placeholder="Harga Barang">
                        </div>
                        <div class="from-group mt-2">
                            <input type="text" id="jumlah" name="jumlah" class="form-control" placeholder="Jumlah Pesanan">
                        </div>
                        <div class="from-group mt-2">
                            <input type="date" id="tglpsn" name="tglpsn" class="form-control" >
                        </div>
                        <div class="from-group mt-2">
                            <button type="submit" class="btn btn-primary">Pesan</button>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </section>   
    
    <hr class="footer">
      <div class="container">
        <div class="row footer-body">
          <div class="col-md-6">
            <div class="Copyright"><var><strong>Copyright</strong><i class=" var fa-Copyright "></i> 2021 - Designed by Norfiah Lailatin~NS</var></p>
            </div>
          </div>
          <div class="col-md-6 d-dlex justify-content-end">
           
          </div>
        </div>
      </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

  </body>
</html>