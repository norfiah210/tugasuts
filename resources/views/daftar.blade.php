<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>kapal jodoh</title>
  </head>
  <body>

  <div class="panel-header">
    <div class="container">
      <h1><marquee ><strong>Welcome To Kapal Jodoh</strong></marquee></h1>
    </div>
  </div>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Kapal Jodoh</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="/">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="dua">Daftar Pesanan</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('pesanan')}}">Pesanan</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div class="container mt-5">
<div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
    <ol class="carousel-indicators">
      <li data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active"></li>
      <li data-bs-target="#carouselExampleDark" data-bs-slide-to="1"></li>
      <li data-bs-target="#carouselExampleDark" data-bs-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <div class="carousel-item active" >
       <center><img src="image/KL.png" class="d-block w-70" alt="First slide image"></center> 
        <div class="carousel-caption d-none d-md-block">
          <h5>WISATA KAPAL JODOH</h5>
          <p><strong>Silahkan Pilih Menu Sesuai Yang Anda Inginkan <br> Enjoy Your Meal</strong> </p>
        </div>
      </div>
      <div class="carousel-item" >
        <center><img src="image/KL.png" class="d-block w-70" alt="Second slide image"></center>
        <div class="carousel-caption d-none d-md-block">
          <h5>WISATA KAPAL JODOH</h5>
          <p>Silahkan Pilih Menu Sesuai Yang Anda Inginkan <br> Enjoy Your Meal </p>
        </div>
      </div>
      <div class="carousel-item">
        <center><img src="image/HG.png" class="d-block w-70" alt="Third slide image"></center>
        <div class="carousel-caption d-none d-md-block">
          <h5>WISATA KAPAL JODOH</h5>
          <p>Silahkan Pilih Menu Sesuai Yang Anda Inginkan <br> Enjoy Your Meal </p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleDark" role="button" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleDark" role="button" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </a>
  </div><br>
  </div>
     
    <a class="left carousel-control" href="#carousel1" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span><span class="sr-only"></span></a><a class="right carousel-control" href="#carousel1" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span><span class="sr-only"></span></a></div>
    <div class="container-fluid">
        <div class="row">
           <div class="col-md-2">
                <div class="card border-dark">
                 <img src="image/gurame.jpg" class="card-img-top" alt="...">
                 <div class="card-body">
                     <h5 class="card-title font-weight-bold">Gurame Goreng</h5>
                     <label class="card-text harga">Rp. 20.000</label><br>
                 </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="card border-dark">
                <img src="image/nasgor.jpg" class="card-img-top" alt="...">
                  <div class="card-body">
                      <h5 class="card-title font-weight-bold">Nasi Goreng</h5>
                      <label class="card-text harga">Rp. 17.000</label><br>
                      </div>
                </div>
        </div>

        <div class="col-md-2">
             <div class="card border-dark">
            <img src="image/asam.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                  <h5 class="card-title font-weight-bold">kakap asam manis</h5>
                  <label class="card-text harga">Rp. 25.000</label><br>
                  </div>
             </div>
         </div>

        <div class="col-md-2">
             <div class="card border-dark">
            <img src="image/tahu.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                  <h5 class="card-title font-weight-bold">Tahu Mozarella</h5>
                  <label class="card-text harga">Rp. 13.000</label><br>
                  </div>
             </div>
        </div>

        <div class="col-md-2">
            <div class="card border-dark">
           <img src="image/kakap.jpg" class="card-img-top" alt="...">
             <div class="card-body">
                 <h5 class="card-title font-weight-bold">Kakap Kecap</h5>
                 <label class="card-text harga">Rp. 22.000</label><br>
                 </div>
            </div>
       </div>
       <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/spagheti.jpg" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Spagethi</h5>
             <label class="card-text harga">Rp. 18.000</label><br>
             </div>
        </div>
      </div>
      <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/ayam geprek.jpg" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Ayam Geprek</h5>
             <label class="card-text harga">Rp. 20.000</label><br>
             </div>
        </div>
      </div>
      <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/udang.jpg" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Udang Pedas</h5>
             <label class="card-text harga">Rp. 18.000</label><br>
             </div>
        </div>
      </div>
      <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/th.jfif" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Pecel Blitar</h5>
             <label class="card-text harga">Rp. 16.000</label><br>
             </div>
        </div>
      </div>
      <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/mF.jfif" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Kepiting Pedas</h5>
             <label class="card-text harga">Rp. 23.000</label><br>
             </div>
        </div>
      </div>
      <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/batagor.jpg" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Batagor</h5>
             <label class="card-text harga">Rp. 15.000</label><br>
            </div>
        </div>
      </div>
      <div class="col-md-2">
        <div class="card border-dark">
       <img src="image/as.jpeg" class="card-img-top" alt="...">
         <div class="card-body">
             <h5 class="card-title font-weight-bold">Mie Bandung</h5>
             <label class="card-text harga">Rp. 15.000</label><br>
             </div>
        </div>
      </div>
   <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/rt.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Machalatte</h5>
         <label class="card-text harga">Rp. 14.000</label><br>
         </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/teh.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Es Teh</h5>
         <label class="card-text harga">Rp. 9.000</label><br>
         </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/ice.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Ice Cream</h5>
         <label class="card-text harga">Rp. 15.000</label><br>
         </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/mt.jfif" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Martabak Telor</h5>
         <label class="card-text harga">Rp. 13.000</label><br>
        </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/macha.jpg" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Milk Shake</h5>
         <label class="card-text harga">Rp. 13.000</label><br>
         </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="card border-dark">
    <img src="image/jus.jfif" class="card-img-top" alt="...">
     <div class="card-body">
         <h5 class="card-title font-weight-bold">Jus Mangga</h5>
         <label class="card-text harga">Rp. 13.000</label><br>
         </div>
    </div>
  </div>
</div>      
    </div>
    <hr class="footer">
      <div class="container">
        <div class="col text-center">
          <div>
            <div class="Copyright"><var><strong>Copyright</strong><i class=" var fa-Copyright "></i> 2021 - Designed by Norfiah Lailatin~NS</var></p>
            </div>
          </div>
         
        </div>
      </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

  </body>
</html>