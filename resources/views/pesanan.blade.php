<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
    <title>kapal jodoh</title>
  </head>
  <body>

  
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Kapal Jodoh</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="/">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="dua">Daftar Pesanan</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('pesanan')}}">Pesanan</a>
        </li>
        </ul>
    </div>
  </div>
</nav>

<div class="panel-header">
    <div class="container">
      <h1><marquee ><strong>Welcome To Kapal Jodoh</strong></marquee></h1>
    </div>
  </div>
     
  <section>
        <div class="content">
            <div class="card card-info card-outline">
                <div class="card-header">
                    <h3 class="alert alert-primary text-center mt-3">Masukkan Pesanan Anda</h3>
                    <a href="{{ route('beli')}}" class="btn btn-success ">PESAN<i class="fas fa-plus-square"></i></a>
                </div>
                
            </div>
        </div>
    </section>
    

    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>No Meja</th>
                <th>Nama Pesanan</th>
                <th>Harga Barang</th>
                <th>Jumlah Pesanan</th>
                <th>Tanggal</th>
                <th>Aksi</th>
            </tr>
            @foreach ($dtpesanan as $item)
            <tr>
                <!-- <td>{{$item->id}}</td> -->
                <td>{{$item->no_meja}}</td>
                <td>{{$item->nama_pesanan}}</td>
                <td>{{$item->harga_barang}}</td>
                <td>{{$item->jumlah_pesanan}}</td>
                <td>{{ $item->tanggal }}</td>
                <td>
                <a href="{{ route('edit', $item->id) }}"><button class="btn btn-info btn-sm" > EDIT </button></a>
                <a href="tiga"><button class="btn btn-success btn-sm" > BAYAR </button></a>
                <form method="POST" action="{{ route('delete', $item->id) }}" id="hapus">
                  @csrf
                  @method('DELETE')
                  <div class="d-grid gap-2">
                  <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('yakin?')">Hapus</button>
                  </div>
              </form> 
          
                </td>
            </tr>
            @endforeach
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </table>
    
    
    <footer>
      <div class="container">
        <div class="col text-center">
        <div class="Copyright"><var><strong>Copyright</strong><i class=" var fa-Copyright "></i> 2021 - Designed by Norfiah Lailatin~NS</var></p>
          </div>
          
        </div>
      </div>
      </footer>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

  </body>
</html>