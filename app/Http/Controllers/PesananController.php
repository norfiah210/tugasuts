<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\pesanan;
use Illuminate\Support\Facades\Redirect;

class PesananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dtpesanan = pesanan::all();
        return view('pesanan',compact('dtpesanan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('beli');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd ($request->all());
        pesanan::create([
            'no_meja'=> $request->no,
            'nama_pesanan'=> $request->nama,
            'harga_barang'=> $request->harga,
            'jumlah_pesanan'=> $request->jumlah,
            'tanggal'=> $request->tglpsn,
        ]);
        return redirect('pesanan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = pesanan::find($id);
        return view('edit' , compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = pesanan::find($id);
        $save = $data->update([
            'no_meja'=> $request->no,
            'nama_pesanan'=> $request->nama,
            'harga_barang'=> $request->harga,
            'jumlah_pesanan'=> $request->jumlah,
            'tanggal'=> $request->tglpsn,
        ]);
        if($save){
            $dtpesanan = pesanan::all();
            return Redirect()->route('pesanan' , compact('dtpesanan'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data =pesanan::find($id);
        $data->delete();
        return Redirect()->route('pesanan');
    }
}
