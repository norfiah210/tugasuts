<?php

use App\Http\Controllers\PesananController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('dua', function () {
    return view('daftar');
});
Route::get('tiga', function () {
    return view('logout');
});


Route::get('/pesanan',[PesananController::class, 'index'])->name ('pesanan') ;
Route::get('/beli','PesananController@create')->name ('beli') ;
Route::get('/simpan','PesananController@store')->name ('simpan') ;
Route::get('/edit/{id}',[PesananController::class , 'edit'])->name ('edit') ;
Route::put('/edit/{id}',[PesananController::class , 'update'])->name ('edit') ;
Route::delete('/delete/{id}',[PesananController::class , 'destroy'])->name ('delete');